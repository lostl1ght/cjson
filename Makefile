parser: parser.o pjson.o cjson.o
	gcc -o $@ $^

%.o: %.c
	gcc -g -c $^

.PHONY: clean

clean:
	rm *.o parser
