#ifndef PJSON_H
#define PJSON_H

#include <stdbool.h>

#include "khash.h"
#include "kvec.h"

//Тип данных json элемента (все допустимые типы в json)
typedef enum {
	PJSON_TYPE_OBJECT,
	PJSON_TYPE_ARRAY,
	PJSON_TYPE_STRING,
	PJSON_TYPE_NUMBER,
	PJSON_TYPE_BOOLEAN,
	PJSON_TYPE_NULL,
} pjson_type_t;

struct pjson_element_s;
typedef struct pjson_element_s pjson_element_t;

KHASH_MAP_INIT_STR(pjson_map, pjson_element_t*);
typedef khash_t(pjson_map) pjson_map_t;
typedef kvec_t(pjson_element_t*) pjson_vec_t; 

//Структура для хранения элемента json. Должна хранить в себе все допустимы типы json элементов: объект, массив, строку, число, булеан
typedef struct pjson_element_s {
	//Содержит тип элемента, в зависимости от типа заполняется одно из полей ниже
	pjson_type_t type;
	union {
		pjson_map_t *object;
		pjson_vec_t *array;
		char *string;
		double number;
		bool boolean;
	};
} pjson_element_t;

//Разбирает json в строке s и возвращает json_element_t
pjson_element_t *pjson_parse(const char *s);
//Освобождает память выделенную под json элемент
void pjson_free(pjson_element_t *element);

//Возвращает элемент json по пути path
//path - строка вида key1.key2.key3, где keyN либо ключ в объекте либо индекс элемента в массиве.
pjson_element_t *pjson_get(pjson_element_t *json, const char *path);

#endif
