#include <stdlib.h>
#include <stdio.h>

#include "cjson.h"
#include "kvec.h"
#include "khash.h"
#include "pjson.h"

//Данные в которых хранится разбираемый json
typedef struct {
	//Стек элементов: объектов и массивов. На вершине хранится объект или массив в который в данный момент идет добавление новые значений 
	kvec_t(pjson_element_t*) elements;
	//Ключ по которому надо добавить следующее значения в объект
	const char *key;
} pjson_data_t;

//Создает новый json element по событию cjson парсера
static pjson_element_t *pjson_create_element(cjson_event_t event, const char *s, const char *end) {

	pjson_element_t *element = calloc(1, sizeof(pjson_element_t));

	switch (event) {
		case CJSON_EVENT_OBEGIN:
			element->type = PJSON_TYPE_OBJECT;
			element->object = kh_init(pjson_map);
			break;
		case CJSON_EVENT_ABEGIN:
			element->type = PJSON_TYPE_ARRAY;
			element->array = calloc(1, sizeof(pjson_vec_t));
			break;
		case CJSON_EVENT_STRING:
			element->type = PJSON_TYPE_STRING;
			element->string = strndup(s, end - s);
			break;
		case CJSON_EVENT_NUMBER:
			element->type = PJSON_TYPE_NUMBER;
			element->number = strtod(s, NULL);
			break;
		case CJSON_EVENT_BOOLEAN:
			element->type = PJSON_TYPE_BOOLEAN;
			element->boolean = (strncmp(s, "true", sizeof("true") - 1) == 0) ? true : false;
			break;
		case CJSON_EVENT_NULL:
			element->type = PJSON_TYPE_NULL;
			break;
	}

	return element;
}

//Добавляет новый элемент value в объект element по ключу key
static void pjson_object_set(pjson_element_t *element, const char *key, pjson_element_t *value) {
	int ret;
	khiter_t k = kh_put(pjson_map, element->object, key, &ret);
	kh_value(element->object, k) = value;
}

//Добавляет элемент value в конец массива element
static void pjson_array_add(pjson_element_t *element, pjson_element_t *value) {
	kv_push(pjson_element_t *, *element->array, value);
}

//Добавляет элемент в данные (в которых строится json)
static void pjson_add_element(pjson_data_t *data, pjson_element_t *element) {

	size_t n_elements = kv_size(data->elements); 
	//Если на верщине стека элементов есть какой-то элемент (объект или массив), то добавляем новый элмент в него
	if (n_elements > 0) {
		pjson_element_t *parent = kv_A(data->elements, n_elements - 1);
		if (parent->type == PJSON_TYPE_OBJECT) {
			pjson_object_set(parent, data->key, element);
			data->key = NULL;
		}
		else if (parent->type == PJSON_TYPE_ARRAY) {
			pjson_array_add(parent, element);
		}
	}
	//Если стек элементов пуст или новый элемент является объектом или массивом, то кладем его на верщину стека, так как в него теперь должы добавляться новые поля
	if (n_elements == 0 || element->type == PJSON_TYPE_OBJECT || element->type == PJSON_TYPE_ARRAY)
		kv_push(pjson_element_t *, data->elements, element);
}

static void pjson_create(cjson_event_t event, const char *s, const char *end, void *pdata) {
	pjson_data_t *data = (pjson_data_t*)pdata;

	switch (event) {
		case CJSON_EVENT_OBEGIN:
		case CJSON_EVENT_ABEGIN: {
			//Если пришло события начала объекта или массива, то создаем его и добавляем в данные
			pjson_element_t *element = pjson_create_element(event, s, end);
			pjson_add_element(data, element);
			break;
		}
		case CJSON_EVENT_OEND:
		case CJSON_EVENT_AEND:
			//Если прило событие окончания объекта или массива, то снимаем его со стека элементов
			kv_pop(data->elements);
			break;
		case CJSON_EVENT_STRING:
		case CJSON_EVENT_NUMBER:
		case CJSON_EVENT_BOOLEAN:
		case CJSON_EVENT_NULL: {
			size_t n_elements = kv_size(data->elements); 
			//Если пришла строка и стек элементов не пуст и на верщине находится объект и мы еще не определили ключ для него, то выставляем текущий ключ ко которому будет добавлено следующее значение.
			if (event == CJSON_EVENT_STRING && n_elements > 0 && kv_A(data->elements, n_elements - 1)->type == PJSON_TYPE_OBJECT && data->key == NULL)
				data->key = strndup(s, end - s);
			else {
			//Иначе создаем элемент по событию и добавляем его в данные
				pjson_element_t *element = pjson_create_element(event, s, end);
				pjson_add_element(data, element);
			}
			break;
	       }

	}
}

pjson_element_t *pjson_parse(const char *s) {
	pjson_data_t data = {0};
	//Вызываем парсер cjson
	if (cjson_parse(s, pjson_create, &data) == -1)
		return NULL;

	//Результатом будет элемент на вершине стека элементов
	pjson_element_t *element = kv_A(data.elements, 0);
	kv_destroy(data.elements);

	return element;
}

void pjson_free(pjson_element_t *element) {
	if (!element)
		return;

	size_t i;
	switch (element->type) {
		case PJSON_TYPE_OBJECT: {
			const char *key;
			const pjson_element_t *value;
			kh_foreach(element->object, key, value, {free((char*)key); pjson_free((pjson_element_t*)value);});
			kh_destroy(pjson_map, element->object);
			break;
		}
		case PJSON_TYPE_ARRAY: {
			size_t i;
			for (i = 0; i < kv_size(*element->array); i++)
				pjson_free(kv_A(*element->array, i));
			free(element->array);
			break;
		}
		case PJSON_TYPE_STRING:
			free(element->string);
			break;
	}
}

//Возвращает элемент из массива element по индексу index
static pjson_element_t *pjson_i(pjson_element_t *element, size_t index) {
	//Проверяем что element дейтсвительно массив
	if (element->type != PJSON_TYPE_ARRAY)
		return NULL;
	//Проверямем что index валиден
	if (index < 0 || index >= kv_size(*element->array))
		return NULL;
	return kv_A(*element->array, index);
}

//Возвращает элемент из объекта element по ключу key
static pjson_element_t *pjson_key(pjson_element_t *element, const char *key) {
	//Проверяем что element действительно объект
	if (element->type != PJSON_TYPE_OBJECT)
		return NULL;
	khiter_t k = kh_get(pjson_map, element->object, key);
	if (k == kh_end(element->object))
		return NULL;
	return kh_value(element->object, k);
}

pjson_element_t *pjson_get(pjson_element_t *element, const char *path) {
	if (!element)
		return NULL;

	const char *p = path;
	const char *path_end = path + strlen(path);

	while (*p) {
		//Ищем следующую часть пути
		const char *end = strchr(p, '.');
		//Если точка не найдена, то берем путь до конца
		if (!end)
			end = path_end;
		//Если ключ это число, то пытаемся получить элемент из массива по индексу
		if (*p > '0' && *p < '9')
			element = pjson_i(element, strtol(p, NULL, 10));
		else {
		//Иначе ключ это строка и пытаемся получить элемент из объекта по ключу
			char key[256];
			snprintf(key, sizeof(key), "%.*s", end - p, p);
			element = pjson_key(element, key);
		}

		if (!element)
			return NULL;

		//Переходим к следующей части ключа и пропускаем точку
		p = end;
		if (*p == '.')
			p++;
	}

	return element;
}
