#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "cjson.h"

//Тип данных для хранения лексемы
typedef enum {
	L_OBEGIN, //{
	L_OEND, //}
	L_ABEGIN, //[
	L_AEND, //]
	L_COLON, //:
	L_COMMA, //,
	L_STRING,
	L_NUMBER,
	L_BOOLEAN,
	L_NULL,
	L_COUNT,
	L_ERR,
} lexem_t;

//Тип даннных для хранения состояния
typedef enum {
	S_BEGIN,
	S_OBJECT,
	S_COLON,
	S_OVALUE,
	S_OCM,
	S_KEY,
	S_ARRAY,
	S_ACM,
	S_AVALUE,
	S_COUNT,
	S_END,
	S_ERR,
} state_t;

//Тип данных для хранения указателя на функцию действия
//Функция действия вызывается при переходе в определенные состояния парсера (см. таблицу переходов).
//s - указатель на начло лексемы, при поступлении которой совершился переход в новое состояние
//**end - указатель на указатель на конец лексемы, а также используется, чтобы сообщить вызывающей функции откуда надо продолжить разбор, после вызова действия. Например get_json переносит нас на конец обрабатываемого подсообщения.
//callback - функция обратного вызова, которая вызывается на определенные переходы (см. структуру cjson_event_t)
//data - данные передаваемые в callback
typedef int (*action_t)(const char *s, const char **end, cjson_callback_t callback, void *data);

static int get_json(const char *s, const char **end, cjson_callback_t callback, void *data);

//Структура правила в таблице переходов
typedef struct {
	state_t state;
	action_t action;
} rule_t;

static int obegin(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_OBEGIN, s, *end, data); return 0; }
static int oend(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_OEND, s, *end, data); return 0; }
static int abegin(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_ABEGIN, s, *end, data); return 0; }
static int aend(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_AEND, s, *end, data); return 0; }
//При вызове колбека с EVENT_STRING передаем строчку без кавычек
static int string(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_STRING, s + 1, *end - 1, data); return 0; }
static int number(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_NUMBER, s, *end, data); return 0; }
static int boolean(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_BOOLEAN, s, *end, data); return 0; }
static int null(const char *s, const char **end, cjson_callback_t callback, void *data) { callback(CJSON_EVENT_NULL, s, *end, data); return 0; }

//Таблица переходов. Для каждого состояния и каждой лексемы определяем правило, которое содержит новое состояние парсера и действие (фукнцию, которую необходимо вызвать при переходе в новое состояние)
//Если осуществляется переход в состоянии ERR или END, то парсинг заканчивается.
const rule_t syntax[S_COUNT][L_COUNT] = {
		/*L_OBEGIN 		L_OEND		L_ABEGIN		L_AEND		L_COLON			L_COMMA		L_STRING		L_NUMBER	L_BOOLEAN	L_NULL*/
/*S_BEGIN*/	{{S_OBJECT, obegin},	{S_ERR, oend},	{S_ARRAY, abegin},	{S_ERR, aend},	{S_ERR, NULL},		{S_ERR, NULL},	{S_END, string},	{S_END, number},{S_END, boolean},{S_END, null}},
/*S_OBEGIN*/	{{S_ERR, NULL},		{S_END, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_COLON, string},	{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL}},
/*S_COLON*/	{{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_OVALUE, NULL},	{S_ERR, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL}},
/*S_OVALUE*/	{{S_OCM, get_json},	{S_ERR, NULL},	{S_OCM, get_json},	{S_ERR, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_OCM, get_json},	{S_OCM, get_json},{S_OCM, get_json},{S_OCM, get_json}},
/*S_OCM*/	{{S_ERR, NULL},		{S_END, oend},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},		{S_KEY, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL}},
/*S_KEY*/	{{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_COLON, string},	{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL}},
/*S_ARRAY*/	{{S_ACM, get_json},	{S_ERR, NULL},	{S_ACM, get_json},	{S_END, aend},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ACM, get_json},	{S_ACM, get_json},{S_ACM, get_json},{S_ACM, get_json}},
/*S_ACM*/	{{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},		{S_END, aend},	{S_ERR, NULL},		{S_AVALUE, NULL},{S_ERR, NULL},		{S_ERR, NULL},	{S_ERR, NULL},	{S_ERR, NULL}},
/*S_AVALUE*/	{{S_ACM, get_json},	{S_ERR, NULL},	{S_ACM, get_json},	{S_ERR, NULL},	{S_ERR, NULL},		{S_ERR, NULL},	{S_ACM, get_json},	{S_ACM, get_json},{S_ACM, get_json},{S_ACM, get_json}},
};

//Возвращает лексему типа строка и устанавливает **end на конец строки.
static lexem_t get_string(const char *s, const char **end) {
	const char *p = s + 1;
	//Идем по строке пока не найдем закрыващую "
	while (*p != '\0' && *p != '"') {
		//Если текущий символ это эскейп символ
		if (*p == '\\') {
			char n = *(p + 1);
			//Проверяем допустимые эскейп последовательности и пропускаем их
			if (n == '"' || n == '\\' || n == '/' || n == 'b' || n == 'f' || n == 'r' || n == 'n' || n == 't')
				p++;
			else if (n == 'u' && isdigit(*(p + 2)) && isdigit(*(p + 3)) && isdigit(*(p + 4)) && isdigit(*(p + 5)))
				p += 5;
			else
				return L_ERR;
		}
		p++;
	}
	//Если не нашли закрывающую ", возвращаем ошибку
	if (*p == '\0')
		return L_ERR;
	*end = p + 1;
	return L_STRING;
}

//Возвращает лексемы: число, булеан или null и устанавливает **end на конец лексемы.
static lexem_t get_number_boolean_null(const char *s, const char **end) {
	lexem_t lexem;
	const char *p = s;
	if (strncmp(p, "true", sizeof("true") - 1) == 0) {
		p += sizeof("true") - 1;
		lexem = L_BOOLEAN;
	}
	else if (strncmp(p, "false", sizeof("false") - 1) == 0) {
		p += sizeof("false") - 1;
		lexem = L_BOOLEAN;
	}
	else if (strncmp(p, "null", sizeof("null") - 1) == 0) {
		p += sizeof("null") - 1;
		lexem = L_NULL;
	}
	else {
		char *e;
		strtod(p, &e);
		if (p == e)
			return L_ERR;
		p = e;
		lexem = L_NUMBER;
	}
	*end = p;
	return lexem;
}

//Возвращает лексему начинающуюся с s и устанавливает end на конец лексемы.
static lexem_t get_lexem(const char *s, const char **end) {
	*end = s + 1;
	switch (*s) {
		case '{': return L_OBEGIN;
		case '}': return L_OEND;
		case '[': return L_ABEGIN;
		case ']': return L_AEND;
		case ':': return L_COLON;
		case ',': return L_COMMA;
		case '"': return get_string(s, end);
		default: return get_number_boolean_null(s, end);
	}
}

//Разбирает json (объект, массив, строку, число, булеан или null)
static int get_json(const char *s, const char **end, cjson_callback_t callback, void *data) {

	//Инициализируем начальное состояние парсера
	state_t state = S_BEGIN;
	while (*s) {
		//Пропускаем незначащие пробельные символы
		while (isspace(*s))
			s++;
		lexem_t lexem = get_lexem(s, end);
		if (lexem == L_ERR)
			return -1;
		//Получаем правило из таблицы переходов
		rule_t rule = syntax[state][lexem];
		//Если переход не допустим, то возвращаем ошибку
		if (rule.state == S_ERR)
			return -1;
		//Если в правиле определено действие, то вызываем его
		if (rule.action != NULL) {
			if (rule.action(s, end, callback, data) == -1)
				return -1;
		}
		//Если достигнуто состояние END, то заверщаем парсинг
		if (rule.state == S_END)
			return 0;
		//Переводим парсер в новое состояние
		state = rule.state;
		//Переходим к началу следующей лексемы
		s = *end;
	}

	return -1;
}

//Разбирает json в строке s, вызывая callback на определенные события (см. структуру cjson_event_t)
int cjson_parse(const char *s, cjson_callback_t callback, void *data) {
	if (!s)
		return -1;
	const char *end;
	if (get_json(s, &end, callback, data) == -1)
		return -1;
	//Пропускаем пробельные символы после разбора json
	while (isspace(*end))
		*end++;
	//Если после json есть какие-то еще данные, то это ошибка
	if (*end != '\0')
		return -1;
	return 0;
}
