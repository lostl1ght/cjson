#ifndef CJSON_H
#define CJSON_H

//События для которых вызываем callback
typedef enum {
	CJSON_EVENT_OBEGIN,
	CJSON_EVENT_OEND,
	CJSON_EVENT_ABEGIN,
	CJSON_EVENT_AEND,
	CJSON_EVENT_STRING,
	CJSON_EVENT_NUMBER,
	CJSON_EVENT_BOOLEAN,
	CJSON_EVENT_NULL,
} cjson_event_t;

typedef void (*cjson_callback_t)(cjson_event_t event, const char *s, const char *end, void *data);

int cjson_parse(const char *s, cjson_callback_t callback, void *data);

#endif
