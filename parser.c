#include <stdio.h>

#include "pjson.h"

int main(int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s json\n", argv[0]);
		return -1;
	}
	pjson_element_t *json = pjson_parse(argv[1]);
	pjson_element_t *element = pjson_get(json, "key3.3.key4");
	if (element && element->type == PJSON_TYPE_NUMBER)
		printf("%f\n", element->number);
	pjson_free(json);
	return 0;
}
